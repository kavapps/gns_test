package com.kavapps.gns.api;

import com.kavapps.gns.pojo.GitHubUsersListResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface GitHubApi {

    @GET("/events")
    Observable<List<GitHubUsersListResponse>> getGitHubUsersList();
}
