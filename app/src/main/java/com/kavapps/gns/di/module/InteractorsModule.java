package com.kavapps.gns.di.module;

import com.kavapps.gns.api.GitHubApi;
import com.kavapps.gns.model.impl.RepoLIstIteractorImpl;
import com.kavapps.gns.model.iteractor.RepoListIteractor;

import dagger.Module;
import dagger.Provides;

@Module
public class InteractorsModule {
    @Provides
    RepoListIteractor provideRepoListIteractor(GitHubApi api) {
        return new RepoLIstIteractorImpl(api);
    }

}