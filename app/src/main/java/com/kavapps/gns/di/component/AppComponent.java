package com.kavapps.gns.di.component;

import com.kavapps.gns.di.module.GitHubApiModule;
import com.kavapps.gns.di.module.InteractorsModule;
import com.kavapps.gns.di.sub.ActivityComponent;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {GitHubApiModule.class})
@Singleton
public interface AppComponent {
    ActivityComponent plusActivityComponent(InteractorsModule interactorsModule);
}
