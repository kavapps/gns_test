package com.kavapps.gns.di.sub;

import com.kavapps.gns.activity.GitHubUsersListActivity;
import com.kavapps.gns.activity.UserRepoInfoActivity;
import com.kavapps.gns.di.module.InteractorsModule;

import dagger.Subcomponent;

@Subcomponent(modules = {InteractorsModule.class})
public interface ActivityComponent {

    void inject(GitHubUsersListActivity gitHubUsersListActivity);

    void inject(UserRepoInfoActivity userRepoInfoActivity);
}
