package com.kavapps.gns.model.iteractor;

import com.kavapps.gns.pojo.GitHubUsersListResponse;

import java.util.List;

public interface RepoListIteractor {

    void getRepoList(RepoListListener listener);
    void destroy();

    interface RepoListListener {
        void onResult(List<GitHubUsersListResponse> list);

        void onError(Throwable throwable);
    }
}
