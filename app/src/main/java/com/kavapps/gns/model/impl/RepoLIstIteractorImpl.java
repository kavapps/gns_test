package com.kavapps.gns.model.impl;

import com.kavapps.gns.api.GitHubApi;
import com.kavapps.gns.model.iteractor.RepoListIteractor;
import com.kavapps.gns.pojo.GitHubUsersListResponse;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class RepoLIstIteractorImpl implements RepoListIteractor {

    private GitHubApi api;
    private Disposable disposable;

    @Inject
    public RepoLIstIteractorImpl(GitHubApi api) {
        this.api = api;
    }



    @Override
    public void getRepoList(RepoListListener listener) {

        disposable = api.getGitHubUsersList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<List<GitHubUsersListResponse>, List<GitHubUsersListResponse>>() {
                    @Override
                    public List<GitHubUsersListResponse> apply(List<GitHubUsersListResponse> listResponses) throws Exception {
                        return listResponses;
                    }
                })
                .subscribe(new Consumer<List<GitHubUsersListResponse>>() {
                    @Override
                    public void accept(List<GitHubUsersListResponse> data) throws Exception {
                        if (listener != null) listener.onResult(data);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (listener != null) listener.onError(throwable);
                    }
                });

    }


    @Override
    public void destroy() {
        if(disposable != null){
            disposable.dispose();
        }
    }

}
