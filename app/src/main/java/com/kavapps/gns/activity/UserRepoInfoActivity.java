package com.kavapps.gns.activity;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Toast;

import com.kavapps.gns.App;
import com.kavapps.gns.R;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

public class UserRepoInfoActivity extends AppCompatActivity implements InternetConnectivityListener {

    @BindView(R.id.webView)
    WebView webView;

    private String userName;
    private final static String GIT_HUB_URL = "https://github.com/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_repo_info);
        ButterKnife.bind(this);

        InternetAvailabilityChecker mInternetAvailabilityChecker;
        InternetAvailabilityChecker.init(this);
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        if (getIntent() != null && getIntent().getExtras() != null) {
            userName = getIntent().getStringExtra("userName");
        }
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(GIT_HUB_URL+userName);

    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (!isConnected) {
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.check_internet),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}