package com.kavapps.gns.activity.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kavapps.gns.R;
import com.kavapps.gns.pojo.GitHubUsersListResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class GitHubAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface GitHubListener{
         void onClickUser(String userName);
    }

    private List<GitHubUsersListResponse> usersList;
    private GitHubListener listener;

    public GitHubAdapter(GitHubListener listener){
        this.listener = listener;
        this.usersList = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.git_hub_repo_item,parent,false);

        return new GitHubViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        GitHubViewHolder gitHubViewHolder = (GitHubViewHolder) holder;
        GitHubUsersListResponse user = usersList.get(position);
        Picasso.with(gitHubViewHolder.itemView.getContext())
                .load(user.getActor().getAvatarUrl())
                .into(gitHubViewHolder.imageUser);
        gitHubViewHolder.userNameText.setText(user.getRepo().getName());
        gitHubViewHolder.typeText.setText(user.getType());
        gitHubViewHolder.dateText.setText(user.getCreatedAt());

        gitHubViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClickUser(user.getActor().getLogin());
            }
        });

    }

    public void setListUsers(List<GitHubUsersListResponse> usersList){
        this.usersList.clear();
        this.usersList.addAll(usersList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public class GitHubViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.imageUser)
        ImageView imageUser;

        @BindView(R.id.userNameText)
        TextView userNameText;

        @BindView(R.id.typeText)
        TextView typeText;

        @BindView(R.id.dateText)
        TextView dateText;


        public GitHubViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
