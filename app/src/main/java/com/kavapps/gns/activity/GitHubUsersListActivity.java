package com.kavapps.gns.activity;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpActivity;
import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kavapps.gns.App;
import com.kavapps.gns.R;
import com.kavapps.gns.activity.ui.GitHubAdapter;
import com.kavapps.gns.pojo.GitHubUsersListResponse;
import com.kavapps.gns.presenter.GitHubUsersListPresenter;
import com.kavapps.gns.view.GitHubUsersListView;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.List;

import javax.inject.Inject;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GitHubUsersListActivity extends MvpActivity implements GitHubAdapter.GitHubListener, GitHubUsersListView, InternetConnectivityListener {

    @BindView(R.id.listUsersRecyclerView)
    RecyclerView listUsersRecyclerView;

    @BindView(R.id.progressCircular)
    ProgressBar progressBar;

    @BindView(R.id.message)
    TextView message;

    @Inject
    @InjectPresenter
    GitHubUsersListPresenter presenter;

    @ProvidePresenter
    GitHubUsersListPresenter providePresenter() {
        return presenter;
    }

    private GitHubAdapter adapter;
    private boolean isInternetConnect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.getActivityComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_git_hub_user_list);
        ButterKnife.bind(this);

        InternetAvailabilityChecker mInternetAvailabilityChecker;
        InternetAvailabilityChecker.init(this);
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);


        adapter = new GitHubAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listUsersRecyclerView.setLayoutManager(mLayoutManager);
        listUsersRecyclerView.setAdapter(adapter);

        progressBar.setVisibility(View.VISIBLE);
        presenter.getListUsers();

    }

    @Override
    public void onClickUser(String userName) {
        if (isInternetConnect){
            Intent intent = new Intent(getApplicationContext(), UserRepoInfoActivity.class);
            intent.putExtra("userName", userName);
            startActivity(intent);
        }else {
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.check_internet),Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void showListUsers(List<GitHubUsersListResponse> gitHubUsersListResponses) {
        progressBar.setVisibility(View.GONE);
        adapter.setListUsers(gitHubUsersListResponses);
    }

    @Override
    public void showMessage(String message) {
        progressBar.setVisibility(View.GONE);
        listUsersRecyclerView.setVisibility(View.GONE);
        this.message.setVisibility(View.VISIBLE);
        this.message.setText(message +"\n"+getResources().getString(R.string.click_to_screen));
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        isInternetConnect = isConnected;
    }

    @OnClick(R.id.relativeLayout)
    void onClick(){
        this.message.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        listUsersRecyclerView.setVisibility(View.VISIBLE);
        presenter.getListUsers();
    }
}