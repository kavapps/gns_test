package com.kavapps.gns.presenter;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.kavapps.gns.model.iteractor.RepoListIteractor;
import com.kavapps.gns.pojo.GitHubUsersListResponse;
import com.kavapps.gns.view.GitHubUsersListView;
import java.util.List;

import javax.inject.Inject;


@InjectViewState
public class GitHubUsersListPresenter extends MvpPresenter<GitHubUsersListView> {

    private RepoListIteractor iteractor;

    @Inject
    public GitHubUsersListPresenter(RepoListIteractor iteractor) {
        this.iteractor = iteractor;
    }


    public void getListUsers() {
        iteractor.getRepoList(new RepoListIteractor.RepoListListener() {
            @Override
            public void onResult(List<GitHubUsersListResponse> list) {
                getViewState().showListUsers(list);
            }

            @Override
            public void onError(Throwable throwable) {
                getViewState().showMessage(throwable.getMessage());
            }
        });
    }

}
