package com.kavapps.gns.view;

import com.arellomobile.mvp.MvpView;
import com.kavapps.gns.pojo.GitHubUsersListResponse;

import java.util.List;

public interface GitHubUsersListView extends MvpView {
    void showListUsers(List<GitHubUsersListResponse> gitHubUsersListResponses);
    void showMessage(String message);
}
